#ifndef TKEYBST
#define TKEYBST
#include "BNode.h"
#include <iostream>
using namespace std;

template <class T, class KEY>
class BST{
	public:
		BNode<T, KEY>* root;
		BST(BNode<T,KEY>* proot = NULL){
			root = proot;
		}
		BNode<T,KEY>* vaciar(BNode<T, KEY>* nodo){
			if (nodo = NULL){ return NULL;}
			vaciar(nodo->left);
			vaciar(nodo->right);
			delete nodo;
			root = NULL;
			return root;
		}
		void insertar(BNode<T,KEY>* &root, BNode<T,KEY>* node){
			if (!root){
				root = node;
				return;
			}
			else{
				KEY keyroot = root->key;
				KEY keynode = node->key;	
				if (keynode < keyroot){
					insertar(root->left, node);
				}
				else{ insertar(root->right, node);}
			}
		}
		BNode<T,KEY>* menor(BNode<T,KEY>* &root){
			if (!root){ return NULL;}
			else if (!root->left){return root;}
			else return menor(root->left);
		}
		BNode<T,KEY>* mayor(BNode<T,KEY>* &root){
			if (!root){ return NULL;}
			else if (!root->right){return root;}
			else return mayor(root->right);
		}
		void remove(KEY &keynode, BNode<T,KEY>* &root){
			if (!root){ 
				return;
			}
			BNode<T,KEY>* temp;
			KEY keyroot = root->key;
			if (keynode < keyroot){
				root->left = remove(keynode, root->left);
			}
			else if (keynode > keyroot){
				root->right = remove(keynode, root->right);
			}
			else if (root->left && root->right){
				temp = menor(root->right);
				root->contenido = temp->contenido;
				root->key = temp->key;
				root->right = remove(root->key, root->right);
			}
			else {
				temp = root;
				if (!root->left){
					root = root->right;
				}
				else if(!root->right){
					root = root->left;
				}
				delete temp;
			}
			return;
		}
		void inorden(BNode<T,KEY>* root){
			if (!root){return;}
			inorden(root->left);
			cout << root->contenido << " ";
			inorden(root->right);
		}
		BNode<T,KEY>* find(BNode<T,KEY>* root, KEY keynode){
			if (!root){
				return NULL;
			}
			KEY keyroot = root->key;
			if (keynode < keyroot){
				return find(root->left,&keynode);
			}
			else if (keynode > keyroot){
				return find(root->right,&keynode);
			}
			else { return root;}
		}
};
				
#endif 		

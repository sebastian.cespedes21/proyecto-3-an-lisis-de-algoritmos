#ifndef TKEYBNODE
#define TKEYBNODE
#include <iostream>

template <class T, class KEY>
class BNode{
	public:
		T contenido;
		KEY key;
		BNode<T, KEY>* right;
		BNode<T, KEY>* left;

		BNode(T pcontenido, KEY pkey, BNode<T, KEY>* pright = NULL, BNode<T, KEY>* pleft = NULL){
			contenido = pcontenido;
			pkey= key;
			right = pright;
			left = pleft;
		}
};
#endif 																																																																							
